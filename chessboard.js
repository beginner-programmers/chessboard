const arg = parseInt(process.argv[2]),
size = Number.isInteger(arg) ? arg : 8;

if (size % 2 !== 0) {return 1;}
if (size < 2) {return 2;}

function generateRowCharacters(size) {
  var rowCharacters = Array(size / 2);

  return rowCharacters.fill("#").join(" ").split("");
}

function offsetLeftOrRight(arr, idx) {
  (idx % 2 === 0) ? arr.unshift(" ") : arr.push(" ");
  return arr;
}

function generateGrid(rowCharacters) {
  var grid = Array(rowCharacters.length + 1).fill(" ");

  return grid.map((el, idx)=>{
    return offsetLeftOrRight(Array.from(rowCharacters), idx);
  });
}

generateGrid(generateRowCharacters(size)).forEach((row)=>{
  console.log(row.join(""));
});

module.exports = {
  generateRowCharacters,
  generateGrid,
  offsetLeftOrRight
};
