### Chessboard exercise from http://eloquentjavascript.net/

The size of the board will default to 8 if a size is not provided.

If an uneven size or size less than two is provided an error code (1 or 2)  is returned.

### Test it
```mocha test```

### Run it
```node chessboard```

```node chessboard 10```
