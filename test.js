const assert = require("assert"),
chessboard = require("./chessboard");

describe("Chessboard", ()=>{
  it("generates a (sized) row of characters minus one for the offset space", ()=>{
    assert.equal(chessboard.generateRowCharacters(8).length, 7);
  });

  it("generates a row with (size / 2) hashes", ()=>{
    var chars = chessboard.generateRowCharacters(8).filter((el)=>{
      return el !== " ";
    });

    assert.equal(chars.length, 4);
  });

  it("offsets a row left for even rows", ()=>{
    var inputArray = ["#", " ", "#"],
    expectedArray = [" ", "#", " ", "#"];

    assert.deepEqual(chessboard.offsetLeftOrRight(inputArray, 0), expectedArray);
  });

  it("offsets a row right for odd rows", ()=>{
    var inputArray = ["#", " ", "#"],
    expectedArray = ["#", " ", "#", " "];

    assert.deepEqual(chessboard.offsetLeftOrRight(inputArray, 1), expectedArray);
  });

  it("generates a grid sized the same as the row size", ()=>{
    assert.equal(chessboard.generateGrid(["#", " ", "#"]).length, 4);
  });
});
